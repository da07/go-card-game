package main

func main() {
	// var card string = "Ace of Spades"
	// := for new variables ==> card = "Five of Diamonds"

	cards := newDeckFromFile("my_cards")
	cards.shuffle()
	cards.print()
	//cards.saveToFile("my_cards")
}
